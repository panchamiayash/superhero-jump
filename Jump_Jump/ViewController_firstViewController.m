//
//  ViewController_firstViewController.m
//  Jump_Jump
//
//  Created by Yash Panchamia on 2/26/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "ViewController_firstViewController.h"

NSString *character = @"batman";
NSString *characterFlipped = @"batman_flipped";

@interface ViewController_firstViewController ()

@end


@implementation ViewController_firstViewController

//- (void)viewDidLoad{
//    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Lego_BG"]];
//    
//   
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)batmanSelector:(UIButton*)sender
{
    character = @"batman";
    characterFlipped = @"batman_flipped";
}

- (IBAction)supermanSelector:(id)sender
{
    character = @"superman";
    characterFlipped = @"superman_flipped";
}

-(void)perform:(id)sender {
    
}

@end
