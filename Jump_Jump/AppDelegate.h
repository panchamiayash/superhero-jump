//
//  AppDelegate.h
//  Jump_Jump
//
//  Created by Yash Panchamia on 2/24/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

