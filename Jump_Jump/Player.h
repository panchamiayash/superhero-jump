//
//  Player.h
//  Jump_Jump
//
//  Created by Yash Panchamia on 2/24/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Player : SKSpriteNode

@property (assign, nonatomic) int velocity;
@property (assign, nonatomic) float xVelocity;

- (void) jump;


@end
