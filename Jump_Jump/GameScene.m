//
//  GameScene.m
//  Jump_Jump
//
//  Created by Yash Panchamia on 2/24/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//
@import CoreMotion;

#import "GameScene.h"
#import "Player.h"
#define GRAVITY -1000
#import <AudioToolbox/AudioToolbox.h>

extern NSString *character;
extern NSString *characterFlipped;

@interface GameScene ()

@property (strong, nonatomic) NSMutableArray *platforms;
@property (strong, nonatomic) Player *player;
@property (strong, nonatomic) SKNode *backgroundNode;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong,nonatomic) SKLabelNode *scoreboard;
@property (strong,nonatomic) SKSpriteNode *boostNode;

@end

bool boost;
//UITouch *touch;

@implementation GameScene
- (void)didMoveToView:(SKView *)view
{
    [self setSize:CGSizeMake(1080, 1920)];
//    for(SKNode *node in self.children)
//    {
//        [node removeFromParent];
//    }
    self.backgroundColor = [UIColor whiteColor];
    self.platforms = [[NSMutableArray alloc] init];
    
    self.backgroundNode = [[SKNode alloc] init];
    [self addChild:self.backgroundNode];
    
    SKTexture *sceneBGImage = [SKTexture textureWithImageNamed:@"black_bg"];
    SKSpriteNode *BgNode = [[SKSpriteNode alloc]initWithTexture:sceneBGImage];
    BgNode.size = self.frame.size;
    BgNode.position = CGPointMake(self.size.width/2,self.size.height/2);
    [self.backgroundNode addChild: BgNode];

    [self setupPlatforms];
    [self setupPlayer];
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = 0.2;
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        [self outputAccelerometerData:accelerometerData.acceleration];
    }];
    
    
    self.scoreboard = [[SKLabelNode alloc]initWithFontNamed:@"Arial"]; // labelNodeWithFontNamed:@"Arial"];
    self.scoreboard.text = @"SCORE:";
    self.scoreboard.fontSize = 50;
    self.scoreboard.position = CGPointMake((self.size.width*0.8),(self.size.height)*0.9);
    
    [self.backgroundNode addChild:self.scoreboard];

    SKTexture *boostImg =[SKTexture textureWithImageNamed:@"Spaceship"];
    self.boostNode=[[SKSpriteNode alloc]initWithTexture:boostImg];
    [self.boostNode setScale:0.5];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(onTimerEvent:) userInfo:nil repeats:YES];
    [self addChild:_boostNode];

}

-(void)onTimerEvent:(NSTimer *) timer
{
    boost = true;
}

- (void) setupPlayer
{
    self.player = [[Player alloc] initWithImageNamed:character];
    self.player.size = CGSizeMake(150, 150);
    [self addChild:self.player];
    self.player.position = CGPointMake(500, 600);
}

- (void) setupPlatforms
{
    for (int i = 0; i < 10; i++) {
        
        SKSpriteNode *platform = [[SKSpriteNode alloc] initWithImageNamed:@"brick_yellow"];
        platform.size=CGSizeMake(200, 50);
        int xOffset = 100 + arc4random() % 550;
        int yOffset = i * 200;
        
        [self.backgroundNode addChild:platform];
        platform.position = CGPointMake(xOffset, yOffset);
        
        [self.platforms addObject:platform];
    }
    
}

- (void) outputAccelerometerData:(CMAcceleration)acceleration
{
    float k = 0.8;
    self.player.xVelocity = k * self.player.xVelocity + (1.0 - k) * acceleration.x;
}



- (void)touchDownAtPoint:(CGPoint)pos {
   
}

- (void)touchMovedToPoint:(CGPoint)pos {
 
}

- (void)touchUpAtPoint:(CGPoint)pos {
 
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    isTouching = true;
//    touch = [touches anyObject];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
  }

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //isTouching = false;
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
   
}


-(void)update:(CFTimeInterval)currentTime
{

    // Called before each frame is rendered
    //NSLog(@"%d",self.player.velocity);
    if(self.player.position.y>0)
    {
    if (self.player.velocity < 0)
    {
        for (SKSpriteNode *node in self.platforms) {
            
            if ([node intersectsNode:self.player])
            {
                [self.player jump];
                SystemSoundID soundID;
                NSString *soundFile = [[NSBundle mainBundle]
                                       pathForResource:@"jump_sound" ofType:@"mp3"];
                AudioServicesCreateSystemSoundID((__bridge  CFURLRef)
                                                 [NSURL fileURLWithPath:soundFile], & soundID);
                AudioServicesPlaySystemSound(soundID);
                // NSLog(@"COLLIDED");
            }
            
            
        }
    }
    if([self.player intersectsNode:self.boostNode])
    {
        NSLog(@"fly");
        self.player.velocity +=4000;
        self.boostNode.hidden = YES;
    }

    
    self.player.velocity = self.player.velocity + GRAVITY * 0.0166;
    //NSLog(@"%f",self.player.xVelocity);
    if(self.player.xVelocity<0)
    {
        self.player.texture = [SKTexture textureWithImageNamed:characterFlipped];
    }
    else
    {
        self.player.texture = [SKTexture textureWithImageNamed:character];
    }
    self.player.position = CGPointMake(self.player.position.x + self.player.xVelocity * 60, self.player.position.y + self.player.velocity * 0.0166);
    
    //self.boostNode.position = CGPointMake(self.player.position.x-20 + self.player.xVelocity * 60, self.player.position.y + self.player.velocity * 0.0166);
    
    if (self.player.position.x > 768) {
        self.player.position = CGPointMake(self.player.position.x - 768, self.player.position.y);
    } else if (self.player.position.x < 0) {
        self.player.position = CGPointMake(768 - self.player.position.x, self.player.position.y);
    }
    
    
    if (self.player.position.y > 500) {
        self.player.position = CGPointMake(self.player.position.x, 500);
        self.boostNode.position = CGPointMake(self.boostNode.position.x, self.boostNode.position.y - self.player.velocity *0.0166);

        for (SKNode *platform in self.platforms) {
            
            platform.position = CGPointMake(platform.position.x, platform.position.y - self.player.velocity * 0.0166);
            
        }
    }
    
    for (SKSpriteNode *platform in self.platforms) {
        if (platform.position.y < -50) {
            
            int xOffset = 100 + arc4random() % 550;
            int yOffset = self.size.height;
            if(boost == true && self.boostNode.position.y <0)
            {
                self.boostNode.hidden = NO;
                self.boostNode.position = CGPointMake(xOffset+20, yOffset+20);
                boost = false;
            }
            platform.position = CGPointMake(xOffset, yOffset);
        }
    }
}
    else
    {
        SKSpriteNode *GameOver = [[SKSpriteNode alloc] initWithImageNamed:@"gameover"];
        GameOver.alpha = 0.5;
        GameOver.size = self.frame.size;
        GameOver.position = CGPointMake(self.size.width/2,self.size.height/2);
        [self addChild:GameOver];
    }
}



@end
