//
//  Player.m
//  Jump_Jump
//
//  Created by Yash Panchamia on 2/24/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "Player.h"

@implementation Player

- (void) jump
{
    self.velocity += 2000;
}

@end
